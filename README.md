# PyCientifico

Curso de Python Científico

## Instalación

- Descargar Anaconda para Python 3.7: https://www.anaconda.com/distribution/#download-section

## Descripción general

Las clases serán abordadas en modalidad taller, se presenta un tema mientras se lo trabaja en forma interactiva, cada uno irá probando 
los distintos fragmentos de código y ejecutando en su computadora. A su vez hay espacios para el autoaprendizaje mediante ejercicios de 
desafío planteados durante la clase.

Cada alumno creará un notebook sobre el que trabajará en cada clase, desde donde programará o probará los códigos contenidos en el notebook 
del tema.

Cada tema estará contenido en un notebook jupyter ``.ipynb`` sobre el que trabajaremos en cada clase y sus archivos de datos correspondientes. 
La forma de comunicación será a través del foro https://groups.google.com/forum/#!forum/pycientifico2019 y un grupo de Telegram.

## Bibliografía

No nos basaremos en un recurso específico, pero en general nos será útil consultar:

- Para las 2 o 3 primeras: https://gitlab.com/emilopez/dev01/tree/master/tuSL/pdfs
- Tutorial oficial de python: http://docs.python.org.ar/tutorial/pdfs/TutorialPython3.pdf
- Luego:
    - https://scipy-lectures.org/
    - Documentación oficial de las bibliotecas:
    - ...

## Tema 1: Python Conceptos Básicos

Tiempo estimado: 2 a 3 clases

- Instalación, exploración de IDEs y notebooks
- Archivos de texto: lectura y escritura
- Parseo de datos: split()
- Tipos de datos: str, int, float
- Contenedores: listas y diccionarios
- Iteraciones: for, while
- Bifurcaciones: if, if-else, if-elif-else
- Funciones de la biblioteca estándar

## Tema 2: Ecosistema Científico

- Numpy, scipy, matplotlib
- Pandas